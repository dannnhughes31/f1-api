<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Http;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/test', function () {
    return response('Ok');
});

Route::get('/fetchData', function () {
    $response = Http::get('http://ergast.com/api/f1/current/last/results');
    return response($response->body())
        ->header('Content-Type', 'application/xml')
        ->header('Access-Control-Allow-Origin', '*')
        ->header('Access-Control-Allow-Methods', 'GET');
});

Route::get('/fetchDrivers', function() {
        $currentYear = date("Y");
        $response = Http::get("http://ergast.com/api/f1/${currentYear}/drivers");
            return response($response->body())
                ->header('Content-Type', 'application/xml')
                ->header('Access-Control-Allow-Origin', '*')
                ->header('Access-Control-Allow-Methods', 'GET');
});

Route::get('/fetchDriver/{id}', function($id) {
    $response = Http::get("http://ergast.com/api/f1/drivers/{$id}");
    return response($response->body())
        ->header('Content-Type', 'application/xml')
        ->header('Access-Control-Allow-Origin', '*')
        ->header('Access-Control-Allow-Methods', 'GET');
});

Route::get('/fetchConstructors', function() {
    $currentYear = date("Y");
        $response = Http::get("http://ergast.com/api/f1/${currentYear}/constructors");
        return response($response->body())
            ->header('Content-Type', 'application/xml')
            ->header('Access-Control-Allow-Origin', '*')
            ->header('Access-Control-Allow-Methods', 'GET');
});


Route::get('/fetchConstructors/{id}', function($id) {
    $currentYear = date("Y");
    $response = Http::get("http://ergast.com/api/f1/${currentYear}/constructors/{$id}");
    return response($response->body())
        ->header('Content-Type', 'application/xml')
        ->header('Access-Control-Allow-Origin', '*')
        ->header('Access-Control-Allow-Methods', 'GET');
});

Route::get('/fetchSchedule', function() {
    $currentYear = date("Y");
    $response = Http::get("http://ergast.com/api/f1/${currentYear}");
    return response($response->body())
        ->header('Content-Type', 'application/xml')
        ->header('Access-Control-Allow-Origin', '*')
        ->header('Access-Control-Allow-Methods', 'GET');
});

Route::get('/fetchRace/{id}', function($id) {
    $currentYear = date("Y");
    $response = Http::get("http://ergast.com/api/f1/${currentYear}/${id}");
    return response($response->body())
        ->header('Content-Type', 'application/xml')
        ->header('Access-Control-Allow-Origin', '*')
        ->header('Access-Control-Allow-Methods', 'GET');
});

Route::get('/fetchResult/{id}', function($id) {
    $currentYear = date("Y");
    $response = Http::get("http://ergast.com/api/f1/${currentYear}/${id}/results");
    return response($response->body())
        ->header('Content-Type', 'application/xml')
        ->header('Access-Control-Allow-Origin', '*')
        ->header('Access-Control-Allow-Methods', 'GET');
});

Route::get('/fetchConstructorStandings', function() {
    $response = Http::get("http://ergast.com/api/f1/current/constructorStandings");
    return response($response->body())
        ->header('Content-Type', 'application/xml')
        ->header('Access-Control-Allow-Origin', '*')
        ->header('Access-Control-Allow-Methods', 'GET');
});

Route::get('/fetchDriverStandings', function() {
    $response = Http::get("http://ergast.com/api/f1/current/driverStandings");
    return response($response->body())
        ->header('Content-Type', 'application/xml')
        ->header('Access-Control-Allow-Origin', '*')
        ->header('Access-Control-Allow-Methods', 'GET');
});


